sap.ui.define(['sap/suite/ui/generic/template/lib/AppComponent'], function(AppComponent) {
    return AppComponent.extend("demo.bookshop.Component", {
        metadata: {
            manifest: "json"
        }
    });
});
